package com.empresa.proyecto.clase;

public class main {

	public static void main(String[] args) {
		float salario, salarioNeto;
		EmpleadoV e1 = new EmpleadoV();
		EmpleadoV e2 = new EmpleadoV();

		try {
			salario = e1.calculaSalarioBruto("vendedor",2000, 3); // pasando argumentos
			salarioNeto = e1.calculaSalarioNeto(1499);
																
			System.out.println("Su salario Bruto es: " + salario);
			System.out.println("Su salario neto es: "+ salarioNeto);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

}
