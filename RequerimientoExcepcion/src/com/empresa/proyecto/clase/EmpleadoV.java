package com.empresa.proyecto.clase;

import com.empresa.proyecto.exception.BRException;

public class EmpleadoV {
	 
	private static final int SALARIO_VENDEDOR = 1000;
	private static final int SALARIO_ENCARGADO = 1500;
	private static final int PAGO_HORAS_EXTRAS = 20;
	private static final double RETENCION_SALARIO_MAYOR_MIL_EUROS_MENOR_MIL_QUINIENTOS = 0.16;
	private static final double RETENCION_SALARIO_MAYOR_MIL_QUINIENTOS_EUROS = 0.18;

	public EmpleadoV() {

	}

	/*
	 * Requerimiento que pide un metodo calcularSalarioBruto si los valores
	 * pasados de los parametros son null o negativos lanza una excepcion
	 * BRException que creamos las excepciones debe manejarse dentro de un try
	 * catch en donde ser�n invocado el metodo
	 */

	/*
	 * Si el salario bruto es menor de 1000 euros, no se aplicar� ninguna
	 * retenci�n. Para salarios a partir de 1000 euros, y menores de 1500 euros
	 * se les aplicar� un 16%, y a los salarios a partir de 1500 euros se les
	 * aplicar� un 18%. El m�todo nos devolver� salarioBruto * (1-retencion), o
	 * BRExcepcion si el salario es menor que cero.
	 */

	public float calculaSalarioBruto(String tipoEmpleado, float ventasMes, float horasExtras) throws BRException {
		float salarioBruto = 0, primaV = 0, primaE = 0;
		if (tipoEmpleado == null || ventasMes < 0 || horasExtras < 0) {
			throw new BRException("Los valores no son los adecuados");
		} else if (tipoEmpleado.equals("vendedor")) {
			if (ventasMes >= 1000) {
				primaV = SALARIO_VENDEDOR + 100;
			} else {
				primaV = SALARIO_VENDEDOR;
			}
			salarioBruto = primaV + (horasExtras * PAGO_HORAS_EXTRAS);

		} else if (tipoEmpleado.equals("encargado")) {
			if (ventasMes >= 1500) {
				primaE = SALARIO_ENCARGADO + 200;
			} else {
				primaE = SALARIO_ENCARGADO;
			}
			salarioBruto = primaE + (horasExtras * PAGO_HORAS_EXTRAS);
		}
		return salarioBruto;
	}

 
	public float calculaSalarioNeto(float salarioBruto)throws BRException {
		float retencion = 0, salarioNeto = 0;
		if(salarioBruto <0){
			throw new BRException("�El salario Bruto no puede ser negativo!");
		}else if (salarioBruto > 1000 && salarioBruto < 1500) {
			retencion = (salarioBruto * (float) RETENCION_SALARIO_MAYOR_MIL_EUROS_MENOR_MIL_QUINIENTOS);
			salarioNeto = salarioBruto - retencion;
		} else if (salarioBruto >= 1500) {
			retencion = (salarioBruto * (float) RETENCION_SALARIO_MAYOR_MIL_QUINIENTOS_EUROS);
			salarioNeto = salarioBruto - retencion;
		}
		return salarioNeto;
	}

}
