package com.empresa.proyecto.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.empresa.proyecto.clase.EmpleadoV;
import com.empresa.proyecto.exception.BRException;

public class EmpleadoVTest {

	/*
	 * El m�todo se ejecutar� antes de cada prueba (antes de ejecutar cada uno
	 * de los m�todos marcados con @Test) Ser� �til para inicializar los datos
	 * de entrada y de salida esperada que se vayan a utilizar en las pruebas
	 */
	private EmpleadoV emp;
	@Before
	public void init() {
		 emp = new EmpleadoV();
	}

	@Test
	public void testCalcularSalarioBruto() {
		
		float salarioReal;
		try {
			salarioReal = emp.calculaSalarioBruto("vendedor", 2000.0f, 3.0f);
			float resultadoEsperado = 1160.0f;
			assertEquals(resultadoEsperado, salarioReal, 0.01);
		} catch (BRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/*
	 * En algunos casos de prueba, lo que se espera como salida no es que el
	 * m�todo nos devuelva un determinado valor, sino que se produzca una
	 * excepci�n. Para comprobar con JUnit que la excepci�n se ha lanzado
	 * podemos optar por dos m�todos diferentes.
	 * 
	 * el mas sencillo indicar la excepcion esperada seguido de @Test (expected
	 * = BRException.class)
	 * 
	 * la otra es utilizar el m�todo fail de JUnit, que nos permite indicar que
	 * hay un fallo en la prueba. En este caso lo que har�amos ser�a llamar al
	 * m�todo probado dentro de un bloque try-catch que capture la excepci�n
	 * esperada. Si al llamar al m�todo no saltase la excepci�n, llamar�amos a
	 * fail para indicar que no se ha comportado como deber�a seg�n su
	 * especificaci�n.
	 */
	@Test
	public void testCalcularSalarioBruto1() { 
		try {
			emp.calculaSalarioBruto(null, 2000, 3);
			fail("Se esperaba excepcion BRException");
		} catch (BRException e) {
		}
	}

}
